#include <SDL2/SDL.h>

#pragma once

class InputHandler_ {
public:
	static InputHandler_* Instance() {
		if (s_pInstance == 0) {
			s_pInstance = new InputHandler_();
			return s_pInstance;
		}
		return s_pInstance;
	}

	void update();
	bool isKeyDown(SDL_Scancode key);

	bool running() {
		return m_bRunning;
	}

private:
	static InputHandler_* s_pInstance;

	void onKeyDown();

	Uint8* keystates;
	bool m_bRunning = true;
};

typedef InputHandler_ InputHandler;



