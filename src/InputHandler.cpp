#include "InputHandler.h"

InputHandler_ *InputHandler_::s_pInstance = 0;

void InputHandler_::update() {
	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			m_bRunning = false;
			break;
		case SDL_KEYDOWN:
			onKeyDown();
			break;
		}
	}
}

void InputHandler_::onKeyDown() {
	keystates = (Uint8*)SDL_GetKeyboardState(0);
}

bool InputHandler_::isKeyDown(SDL_Scancode key) {
	if (!keystates) {
		return false;
	}

	return keystates[key] == 1;
}
