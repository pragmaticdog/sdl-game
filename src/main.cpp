#include <iostream>
#include <SDL2/SDL.h>
#include "Game.h"
//#include<Windows.h>

const int FPS = 60;
const int DELAY_TIME = 1000.0f / FPS;

Game* g_game = 0;
int main(int argc, char* args[])
{
	Uint32 frameStart, frameTime;

	short error;

	Game::Instance()->init("game", 100, 100, 640, 580, false);
	error = Game::Instance()->load_media();
	if (error == 0) {
		std::cout << "\nError loading textures\n";
	}
	while (Game::Instance()->running()) {
		frameStart = SDL_GetTicks();

		Game::Instance()->handleEvents();
		Game::Instance()->update();
		Game::Instance()->render();
		
		frameTime = SDL_GetTicks() - frameStart;

		if (frameTime < DELAY_TIME) {
			SDL_Delay((int) (DELAY_TIME - frameTime));
		}
	}
	
	Game::Instance()->clean();

	return 0;
}

