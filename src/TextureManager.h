#include <SDL2/SDL.h>
#include<iostream>
#include<map>

#pragma once

class TextureManager_ {
private:
	static TextureManager_* s_pInstance;
	TextureManager_(){}
	std::map<std::string, SDL_Texture*> m_textureMap;

public:
	static TextureManager_* Instance() {
		if (s_pInstance == 0) {
			s_pInstance = new TextureManager_();
			return s_pInstance;
		}
		return s_pInstance;
	}

	bool load(std::string filename, std::string id, SDL_Renderer* pRenderer);

	void draw(std::string id, int x, int y, int width,
		int height, SDL_Renderer* pRenderer,
		SDL_RendererFlip flip = SDL_FLIP_NONE);

	void drawFrame(std::string id, int x, int y, int width,
		int height, int currentRow, int currentFrame,
		SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);

  void drawTile(std::string id, int margin, int spacing, int x, int y,
    int width, int height, int currentRow, int currentFrame, SDL_Renderer *pRenderer);
};

typedef TextureManager_ TextureManager;

