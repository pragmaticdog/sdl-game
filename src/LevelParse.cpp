#include "LevelParse.hpp"
#include "tinyxml2.h"
#include "Level.hpp"
#include "TextureManager.h"
#include "Game.h"
#include "Tileset.hpp"
#include "zlib.h"
#include "Tilelayer.hpp"
#include "base64.h"
#include <stdlib.h>

using namespace tinyxml2;

Level *LevelParser::parseLevel(const char *levelFile){
  // open the file
  XMLDocument levelDocument;
  levelDocument.LoadFile(levelFile);

  //create the level object
  Level *pLevel = new Level();

  // get the root node
  XMLElement *pRoot = levelDocument.RootElement();
  m_tileSize = atoi(pRoot->Attribute("tilewidth"));
  m_width = atoi(pRoot->Attribute("width"));
  m_height = atoi(pRoot->Attribute("height"));

  // parse the tileset
  for(XMLElement *e = pRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
    if(e->Value() == std::string("tileset")){
      parseTilesets(e, pLevel->getTilesets());
    }
  }

  // parse any object layers
  for(XMLElement *e = pRoot->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
    if(e->Value() == std::string("layer")){
      parseTileLayer(e, pLevel->getLayers(), pLevel->getTilesets());
    }
  }

  return pLevel;
}

void LevelParser::parseTilesets(XMLElement *pTilesetRoot, std::vector<Tileset> *pTilesets){
  // load to texture manager
  TextureManager::Instance()->load(pTilesetRoot->FirstChildElement()->Attribute("source"),
                                   pTilesetRoot->Attribute("name"), Game::Instance()->getRenderer());

  Tileset tileset;
  tileset.width = atoi(pTilesetRoot->FirstChildElement()->Attribute("width"));
  tileset.height = atoi(pTilesetRoot->FirstChildElement()->Attribute("height"));
  tileset.firstGridID = atoi(pTilesetRoot->Attribute("firstgid"));
  tileset.tileWidth = atoi(pTilesetRoot->Attribute("tilewidth"));
  tileset.height = atoi(pTilesetRoot->Attribute("tileheight"));
  tileset.spacing = atoi(pTilesetRoot->Attribute("spacing"));
  tileset.margin = atoi(pTilesetRoot->Attribute("margin"));

  tileset.name = pTilesetRoot->Attribute("name");

  tileset.numColumns = tileset.width / (tileset.tileWidth + tileset.spacing); 
  pTilesets->push_back(tileset);
}

void LevelParser::parseTileLayer(XMLElement *pTileElement, std::vector<Tilelayer*> *pLayers, const std::vector<Tileset> *pTilesets){
  Tilelayer *pTileLayer = new Tilelayer(m_tileSize, *pTilesets);

  std::vector<std::vector<int>> data;
  std::string decodedIDs;
  XMLElement* pDataNode;

  for(XMLElement *e = pTileElement->FirstChildElement(); e != NULL; e = e->NextSiblingElement()){
    if(e->Value() == std::string("data")){
      pDataNode = e;
    }
  }

  for(XMLNode *e = pDataNode->FirstChild(); e!=NULL; e = e->NextSibling()){
    XMLText *text = e->ToText();
    std::string t = text->Value();
    decodedIDs = base64_decode(t);
  }

  // uncompress zlib compression
  uLongf numGids = m_width * m_height * sizeof(int);
  std::vector<unsigned> gids(numGids);
  uncompress((Bytef*)&gids[0], &numGids, (const Bytef*) decodedIDs.c_str(), decodedIDs.size());

  // TODO: find a better way of allocating space
  // allocating data for adding actual data
  std::vector<int> layerRow(m_width);
  for(int j=0; j < m_width; j++){
    data.push_back(layerRow);
  }
  // adding actual data
  for(int rows = 0; rows < m_height; rows++){
    for(int cols = 0; cols < m_width; cols++){
      std::cout<<gids[rows * m_width + cols];
      data[rows][cols] = gids[rows * m_width + cols];
    }
  }
  pTileLayer->setTileIDs(data);
  pLayers->push_back(pTileLayer);
}












