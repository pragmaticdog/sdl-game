#pragma once
#include "Tileset.hpp"
#include "Tilelayer.hpp"
#include <vector>

class Level{
public:
  Level(){}
  ~Level(){}

  void update();
  void render();
    
  std::vector<Tileset> *getTilesets(){
    return &m_tilesets;
  }

  std::vector<Tilelayer*> *getLayers(){
    return &m_layers;
  }
private:
  friend class LevelParser;
  std::vector<Tileset> m_tilesets;
  std::vector<Tilelayer*> m_layers;
};


