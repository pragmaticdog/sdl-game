#include "Game.h"
#include "GameObject.h"
#include "TextureManager.h"
#include "InputHandler.h"

// SDLGameObject defenition
SDLGameObject::SDLGameObject(const LoaderParams* pParams):
	GameObject(pParams), m_position(pParams->getX(), pParams->getY()) {
	m_width = pParams->getWidth();
	m_height = pParams->getHeight();
	m_textureID = pParams->getTextureID();

	m_currentFrame = 1;
	m_currentRow = 1;	
}

void SDLGameObject::draw() {
	TextureManager::Instance()->drawFrame(m_textureID, (int)m_position.getX(), (int)m_position.getY(), m_width, m_height, 
											m_currentRow, m_currentFrame, Game::Instance()->getRenderer());
}

void SDLGameObject::update() {}
// Player defenition
Player::Player(const LoaderParams* mParams) : 
	SDLGameObject(mParams), m_velocity(0,0) {}

void Player::draw() {
	SDLGameObject::draw();
}

void Player::update() {
	handleInput();
	//m_position += m_velocity;
	m_currentFrame = int(((SDL_GetTicks() / 100) % 6));
}

void Player::handleInput() {
	// test handling
	// TODO: convert this if ladder into a switch
	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT)) {
		m_position.setX(m_position.getX() - 1);
	}
	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT)) {
		m_position.setX(m_position.getX() + 1);
	}
	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_UP)) {
		m_position.setY(m_position.getY() - 1);
	}
	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_DOWN)) {
		m_position.setY(m_position.getY() + 1);
	}
}

// Enemy defenition
Enemy::Enemy(const LoaderParams* mParams) : SDLGameObject(mParams) {}

void Enemy::update() {
	m_currentFrame = int(((SDL_GetTicks() / 100) % 6));
}

void Enemy::draw(){
	SDLGameObject::draw();
}
