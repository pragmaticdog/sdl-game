#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "TextureManager.h"
#include "GameObject.h"
#include "Level.hpp"

#pragma once

enum game_states {
	MENU,
	PLAY,
	GAMEOVER
};
class Game_ {
public: 
	static Game_* Instance() {
		if (s_pInstance == 0) {
			s_pInstance = new Game_();
			return s_pInstance;
		}
		return s_pInstance;
	}

	SDL_Renderer* getRenderer() const {
		return m_pRenderer;
	}

	// initialize the window
	bool init(const char* title, int xpos, int ypos,
		int height, int width, bool fullscreen);
	
	// render window
	void render();

	void update();
	void handleEvents();
	void clean();
	bool load_media();

	bool running() {
		return m_bRunning;
	}
  
  int getGameWidth() const{
    return m_gameWidth;
  }

  int getGameHeight() const{
    return m_gameWidth;
  }

private:
	static Game_* s_pInstance;

	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;
	
	int m_currentFrame;
	
	std::vector	<GameObject*> m_gameObjects;
	GameObject* m_player;
	GameObject* m_enemy;

	bool m_bRunning;
  
  int m_gameHeight;
  int m_gameWidth;

  Level *pLevel;
};

// the singleton instance
typedef Game_ Game;

