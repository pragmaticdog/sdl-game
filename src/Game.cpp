#include <iostream>
#include "Game.h"
#include "InputHandler.h"
#include "LevelParse.hpp"

Game* Game_::s_pInstance = 0;

bool Game_::load_media() {
	TextureManager::Instance()->load("assets/animate-alpha.png", "animate", m_pRenderer);
	return 1;
}

bool Game_::init(const char* title, int xpos, int ypos,
	int height, int width, bool fullscreen)
{
	m_player = new Player(new LoaderParams(100, 100, 128, 82, "animate"));
	m_enemy = new Enemy(new LoaderParams(300, 300, 128, 82, "animate"));

	m_gameObjects.push_back(m_player);
	m_gameObjects.push_back(m_enemy);
  
  m_gameHeight = height;
  m_gameWidth = width;
	
  LevelParser levelParser;
  pLevel = levelParser.parseLevel("assets/map.tmx");
    
  if (SDL_Init(SDL_INIT_EVERYTHING) >= 0) {
		std::cout << "SDL init success\n";
		
		// create the window
		int flags = fullscreen ? SDL_WINDOW_FULLSCREEN : 0;
		m_pWindow = SDL_CreateWindow(title, xpos, ypos, height, width, flags);

	
		if (m_pWindow != 0) {
			std::cout << "Window creation success\n";
			m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);

			if (m_pRenderer != 0) {
				std::cout << "render creation success\n";
				//SDL_SetRenderDrawColor(m_pRenderer, 255, 0, 0, 255);
			}
			else {
				std::cout << "render init fail\n";
				return false;
			}
		}
		else {
			std::cout << "window init fail\n";
		}
	}
	else {
		std::cout << "std init fail";
		m_bRunning = false;
	}
	m_bRunning = true;
	std::cout << "init success\n";

	return true;
}

void Game_::render() {
	SDL_RenderClear(m_pRenderer);

	for (std::vector<GameObject*>::size_type i = 0; i != m_gameObjects.size(); i++) {
		m_gameObjects[i]->draw();
	}

  pLevel->render();
	
  SDL_RenderPresent(m_pRenderer);
}

void Game_::update() {
	for(std::vector<GameObject*>::size_type i = 0; i != m_gameObjects.size(); i++){
		m_gameObjects[i]->update();
	}
  pLevel->update();
}

void Game_::handleEvents() {
	InputHandler::Instance()->update();
	if (InputHandler::Instance()->running() == false) {
		m_bRunning = false;
	}
}

void Game_::clean() {
	std::cout << "cleaning game\n";
	SDL_DestroyWindow(m_pWindow);
	SDL_DestroyRenderer(m_pRenderer);
	SDL_Quit();
}
