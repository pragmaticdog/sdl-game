#include<iostream>
#include <SDL2/SDL.h>
#include "Vector2D.h"
#pragma once

// LoadParams class
class LoaderParams {
public:
	LoaderParams(int x, int y, int width, int height, std::string textureID) :
		m_x(x), m_y(y), m_width(width), m_height(height), m_textureID(textureID) {}

	// getters
	int getX() const{
		return m_x;
	}
	
	int getY() const{
		return m_y;
	}

	int getWidth() const{
		return m_width;
	}

	int getHeight() const{
		return m_height;
	}

	std::string getTextureID() const{
		return m_textureID;
	}
private:
	int m_x;	
	int m_y;

	int m_width;
	int m_height;

	int m_currentFrame;
	int m_currentRow;

	std::string m_textureID;
};


// GameObject class
class GameObject {
public:
	virtual void draw() = 0;
	virtual void update() = 0;
	// virtual void clean();

protected:
	GameObject(const LoaderParams* pParams) {}
	virtual ~GameObject() {}
};

class SDLGameObject : public GameObject {
public:
	SDLGameObject(const LoaderParams* pParams);

	virtual void draw();
	virtual void update();
	// virtual update clean();

protected:
	Vector2D m_position;

	int m_width;
	int m_height;

	int m_currentRow;
	int m_currentFrame;

	std::string m_textureID;
};

// Player class
class Player : public SDLGameObject {
public:
	Player(const LoaderParams* pParams);

	virtual void draw();
	virtual void update();
	// void clean();

private:
	Vector2D m_velocity;

	void handleInput();
};

// Enemy class
class Enemy : public SDLGameObject {
public:
	Enemy(const LoaderParams* mParams);

	virtual void draw();
	virtual void update();
	// void clean();
};

