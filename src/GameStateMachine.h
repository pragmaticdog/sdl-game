#include "GameState.h"
#include<vector>

#pragma once

class GameStateMachine {
public:
	void pushState(GameState* pState);
	void changeState(GameState* oState);
	void popState();

private:
	std::vector<GameState*> m_gameStates;
};