#include "TextureManager.h"
#include <SDL2/SDL_image.h>

TextureManager_*	TextureManager::s_pInstance = 0;

bool TextureManager_::load(std::string filename, std::string id, SDL_Renderer* pRenderer) {
	
	SDL_Surface* pTempSurface = IMG_Load(filename.c_str());
	if (pTempSurface == 0)
		return false;

	SDL_Texture* pTexture = SDL_CreateTextureFromSurface(pRenderer, pTempSurface);
	SDL_FreeSurface(pTempSurface);

	if (pTexture != 0) {
		m_textureMap[id] = pTexture;
		return true;
	}

	return false;
}

void TextureManager_::draw(std::string id, int x, int y, int width,
	int height, SDL_Renderer* pRenderer,
	SDL_RendererFlip flip) {
	
	SDL_Rect srcRect;
	SDL_Rect destRect;

	srcRect.x = 0;
	srcRect.y = 0;
	srcRect.w = destRect.w = width;
	srcRect.h = destRect.h = height;
	
	destRect.x = x;
	destRect.y = y;

	SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, flip);
}

void TextureManager_::drawFrame(std::string id, int x, int y, int width,
	int height, int currentRow, int currentFrame,
	SDL_Renderer* pRenderer, SDL_RendererFlip flip) {

	SDL_Rect srcRect;
	SDL_Rect destRect;
	
	srcRect.x = width * currentFrame;
	srcRect.y = height * (currentRow - 1);
	srcRect.w = destRect.w = width;
	srcRect.h = destRect.h = height;
	destRect.x = x;
	destRect.y = y;

	SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, flip);
}

void TextureManager_::drawTile(std::string id, int margin, int spacing, int x, int y, int width, int height,
    int currentRow, int currentFrame, SDL_Renderer *pRenderer){
  
  SDL_Rect srcRect;
  SDL_Rect destRect;

  srcRect.x = margin + (spacing + width) * currentFrame;
  srcRect.y = margin + (spacing + width) * currentRow;
  srcRect.h = destRect.h = height;
  srcRect.w = destRect.w = width;
  destRect.x = x;
  destRect.y = y;

  SDL_RenderCopyEx(pRenderer, m_textureMap[id], &srcRect, &destRect, 0, 0, SDL_FLIP_NONE);
}

